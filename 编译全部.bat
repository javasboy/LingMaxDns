set bin_path="./bin/"
set bin_name=LingMaxDns
set code_file="main.go"
mkdir %bin_path%


set CGO_ENABLED=0 
set GOPROXY="https://goproxy.io,direct"


set GOOS=windows
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH%.exe %code_file%

set GOOS=windows
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH%.exe %code_file%


set GOOS=android
set GOARCH=arm
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=darwin
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=darwin
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=darwin
set GOARCH=arm
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=darwin
set GOARCH=arm64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=dragonfly
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=freebsd
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=freebsd
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=freebsd
set GOARCH=arm
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=arm
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=arm64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=ppc64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=ppc64le
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=mips
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=mipsle
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=mips64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=linux
set GOARCH=mips64le
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=netbsd
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=netbsd
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=netbsd
set GOARCH=arm
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=openbsd
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=openbsd
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=openbsd
set GOARCH=arm
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=plan9
set GOARCH=386
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=plan9
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%

set GOOS=solaris
set GOARCH=amd64
go build -o %bin_path%%bin_name%_%GOOS%_%GOARCH% %code_file%


