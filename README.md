# LingMaxDns

#### 介绍
解决宽带自身DNS造成的网络延迟高问题

原理: 找出网站多个区域的服务器ip 找出你的网线访问最快那个ip使用 短时间缓存

#### 软件架构
纯GO代码 代码实验性质 

tcp模块+udp模块实现    不依赖第三方库插件  编译出来仅仅2mb

```
市场上已有SmartDns,为什么还要发布这个DNS插件呢?  (配置复杂 修修改改都没见效  选取ip机制呆板)
对比有哪些优化呢
1.原有DNS获取的ip延迟>40ms才会启用优化(小于40ms的几乎都是运营商优化过了的节点)
2.免配置 直接启动转发端口即可 全球30多个tcpDNS+全球阿里httpDNS  (可以使用MIXBOX工具箱一键安装)
```

前言:
```
本人这边是长城网络
3秒断一次tcp极差的那种网络环境
打开淘宝,美团各大APP都是白屏
各种时不时随机Lodin....
狠下心来 钻研到底 写个路由器版本
```





 **功能效果参考:** 

```
1秒开Github  (长城宽带除外)
苹果手机秒开AppStore  ok
微软商店打开不用转圈圈等待  ok
酷狗在线听新歌都直接进入播放 不卡顿 ok
刷淘宝不用等加载 ok
浏览器  网页都是秒开  提速明显 ok
全部都是tcp协议DNS  杜绝DNS劫持  覆盖全球各地 在哪个国家都能享受到一样的加速
```

2022-2-17  发布第一个版本

2022-5-21  增加守护进程避免闪退,支持HTTPS代理,优化加速算法提升

版本发布 路由内嵌研究讨论  HOT 2万+

https://www.right.com.cn/forum/thread-8137820-1-1.html

 **成品下载** 

[下载地址https://wwi.lanzouj.com/b011aogd](https://wwi.lanzouj.com/b011aogdg)

密码:degz


软件默认端口8287  https代理(TCP)/DNS(UDP)   作者by:LingMax  开发不易 请善用

找到自己设备cpu支持的程序  (静态编译不依赖任何运行库)
我的是红米RMAC2100  cpu是mipsle指令结构  我选择LingMaxDns_linux_mipsle文件

## 安装方法

```
# 拷贝到
/etc/LingMaxDns_linux_mipsle

# 设置可执行权限
chmod 755 /etc/LingMaxDns_linux_mipsle
```

### 以openwrt举例

管理后台->网络->防火墙->自定义规则  (配置全部经过路由53端口的DNS数据包都转发到8287端口  或者配置上游dnsmaq也行)
```
iptables -t nat -A PREROUTING -p udp --dport 53 -j REDIRECT --to-ports 8287
```

保存设置

直接执行测试看看有没有报错

```
/etc/LingMaxDns_linux_mipsle
```

### 开机启动方案1 (二选一)

管理后台->系统->启动项->本地启动脚本
```
/etc/LingMaxDns_linux_mipsle &
```

### 开机启动方案2 (二选一)
```
# ssh执行
ln -s /etc/LingMaxDns_linux_mipsle /etc/rc.d/S999LingMaxDns
```

### 启动参数

默认当前程序是守护程序 (会自动创建一个-D主程序进行服务  -D进程报错闪退关闭后会 被守护进程重启)
```
 -D 直接启动服务 半小时后自动结束(不建议用 半小时就关闭了)
-P53 改用53端口监听
```

针对国外大型网站/以及国内视频网站   访问有奇效! 

有兴趣的可以 自己打包下系统包/插件包发布玩去

我自己已经解决网络延迟高问题  后面只管杀,不管埋
